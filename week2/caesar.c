#include <stdio.h>
#include <stdlib.h>

int get_plain_str(void);

char plain[50];
int main(int argc, char* argv[])
{
    char cipher[50];

    if (argc != 2)
    {
        printf("Exactly one cli argument is required.\n");
        return 1;
    }
    int k = atoi(argv[1]);
    printf("plaintext: ");
    int len;
    len = get_plain_str();
   
    for (int i=0; i < len; i++)
    {
        if (plain[i] >= 'A' && plain[i] <= 'Z')
        {
            cipher[i] = (plain[i] + k - 'A') % 26 + 'A';
        }
        else if (plain[i] >= 'a' && plain[i] <= 'z')
        {

            cipher[i]  = (plain[i] + k - 'a') % 26 + 'a';
        }
        else 
            cipher[i] = plain[i];

    }
    printf("ciphertext: %s\n", cipher);

    return 0;
}

int get_plain_str(void)
{
    int c;
    int j = 0;
    for (int i = 0; (c = getchar()) != EOF; i++)
    {
        if (c != '\n')    
        {
            plain[i] = c;
            j++;
        }
        else
            break;
    }
    return j;
}
