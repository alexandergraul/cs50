#include <stdio.h>

/*
 * row n: n+1 #s
 * row n-1: n #s
 * row n-2: n-1 #s
 * ...
 * spaces = n+1 - #s
 */

int get_height(void);


int main(void)
{
    int height = get_height();
    if (height < 0 || height > 23)
    {
        height = get_height();
    } else if (height == 0)
        return 0;

    for (int i = 0; i <= height - 1; i++)
    {
        int spaces = height - i;
        int hashes = height - spaces;
        for (int s = 0; s < spaces - 1; s++)
            printf(" ");
        for(int h = 0; h <= hashes; h++)
            printf("#");
        printf("   ");
        for(int h = 0; h <= hashes; h++)
            printf("#");
        printf("\n");
    }

}




int get_height(void)
{
    int n;
    printf("Height: ");
    scanf("%d", &n);
    return n;
}
