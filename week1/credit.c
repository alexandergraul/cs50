#include <stdio.h>

long long get_cc_number(void);
int main(void)
{
    long long cc_number = get_cc_number();

    int sum_first = 0;
    int sum_second = 0;
    int alternate = 0;
    int c;
    int sum_total;
    int first_digit, second_digit;

    while (cc_number > 0)
    {
        // take of last digit and save it in c
        c = cc_number % 10;

        if (cc_number < 100 && cc_number > 10)
            second_digit = c;
        else if (cc_number < 10)
            first_digit = c;

        cc_number = cc_number / 10;

        // save alternating digits in two variables
        if (alternate == 0)
        {
            sum_first += c;
            alternate = 1;
        }
        else if (alternate == 1)
        {
            c = c * 2;

            if (c >= 10)
            {
                sum_second += c % 10;
                sum_second += c / 10;
            }
            else
                sum_second += c;

            alternate = 0;
        }
    }


    sum_total = sum_first + sum_second;
    if(sum_total % 10 == 0 && first_digit == 4)
        printf("VISA\n");
    else if (sum_total % 10 == 0 && first_digit == 5 && second_digit <= 5)
            printf("MASTERCARD\n");
    else if (sum_total % 10 == 0 && first_digit == 3
        && (second_digit == 4 || second_digit == 7))
            printf("AMEX\n");
    else
        printf("Invalid.\n");
}

long long get_cc_number(void)
{
    printf("Enter CC number: ");
    long long cc;
    scanf("%lld", &cc);

    return cc;
}
