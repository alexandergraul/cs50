#include <stdio.h>

int get_int();

int main(void)
{
    printf("Minutes: ");
    int n = get_int();
    printf("\nBottles: %d", n * 12);
}

int get_int(void)
{
   int n;
   scanf("%d", &n);
   return n;
}
