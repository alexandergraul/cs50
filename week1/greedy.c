#include <stdio.h>

int change(int old);

int c50, c20, c10, c5, c2, c1;


int main(void)
{
    float float_change;
    int   int_change;
    c50 = c20 = c10 = c5 = c2 = c1 = 0;
    float_change = 3.85;
    int_change = (int) (float_change * 100);

    while (int_change > 0)
    {

        printf("50: %i\n20: %i\n10: %i\n5: %i\n2: %i\n1: %i\n", c50, c20, c10,
           c5, c2, c1);
        printf("Rest change: %i\n", int_change);
        int_change = change(int_change);
    }
    printf("Final:\t50: %i\t20: %i\t10: %i\t5: %i\t2: %i\t1: %i\n", c50, c20,
           c10, c5, c2, c1);
}

int change(int old)
{
    if (old >= 50)
    {
        old = old - 50;
        c50++;
    } else if (old >= 20)
    {
        old = old - 20;
        c20++;
    } else if (old >= 10)
    {
        old = old - 10;
        c10++;
    } else if (old >= 5)
    {
        old = old - 5;
        c5++;
    } else if (old >= 2)
    {
        old = old - 2;
        c2++;
    } else
    {
        old = old - 1;
        c1++;
    }
    return old;
}
